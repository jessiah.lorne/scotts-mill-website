module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg : grunt.file.readJSON('package.json'),

    sass : {
      dist: {
          files: {
              'site/content/css/custom_style.css':'scss/styles.scss'
          }
      }
    },

    assemble: {
      project: {
        options: {
          layout: "content/templates/basic.hbs",
          partials: "content/partials/**/*.hbs" 
        },
        files: {
          'site/': ["content/*.hbs" ]
        }
      }
    },

    clean: {
      build: ["site/"]
    },

    copy: {
      final: {
        files: [{expand: 'true', cwd: 'site/content/', src: '**', dest: '../ftp_files/'}]
      },
      test: {
        files: [{expand: 'true', cwd: 'site/content/', src: '**', dest: '../ftp_files/2017_preview/'}]
      },
    }

  });


  grunt.loadNpmTasks('grunt-assemble');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  // Default task(s).
  //
  grunt.registerTask('speak',function() {
      var msg = "Doing all right";

      grunt.log.write(msg);
  });
  grunt.registerTask('gtfo', ['clean:node']);
  grunt.registerTask('styling',['sass']);
  grunt.registerTask('build', ['clean', 'sass', 'assemble']);
  grunt.registerTask('deploy', ['copy:final']);
  grunt.registerTask('proto', ['copy:test']);

};